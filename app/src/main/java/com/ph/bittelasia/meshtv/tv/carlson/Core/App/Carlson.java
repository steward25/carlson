package com.ph.bittelasia.meshtv.tv.carlson.Core.App;


import android.os.AsyncTask;
import android.util.Log;

import com.ph.bittelasia.meshtv.tv.carlson.R;
import com.ph.bittelasia.meshtv.tv.meshxmpplibrary.Core.Control.Manager.MeshXMPPManager;
import com.ph.bittelasia.meshtv.tv.meshxmpplibrary.Core.Control.Preference.MeshXMPPPreference;
import com.ph.bittelasia.meshtv.tv.meshxmpplibrary.Core.Model.MeshXMPPUser;
import com.ph.bittelasia.meshtv.tv.meshxmpplibrary.DataListener.MeshTVMessageListener;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.API.GET.ReportAnalytics;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.API.Listeners.MeshRequestListener;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.API.POST.MeshRegisterTask;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Annotation.Event.Init;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Annotation.Settings.AppSettings;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Annotation.Settings.RealmEditorSettings;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Constant.AppDataSource;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Data.Control.Listener.MeshDataListener;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Data.Control.Manager.MeshTVDataManager;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Preference.Control.Manager.MeshTVPreferenceManager;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Preference.Model.MeshConfig;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Preference.Model.MeshGuest;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Preference.Model.MeshWeatherLocal;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Util.ResourceManager.MeshResourceManager;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.View.App.MeshTVApp;
import com.ph.bittelasia.meshtv.tv.mtvlib.Data.Model.MeshData;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Model.Bill.MeshBillV2;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Model.Channel.MeshChannel;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Model.Channel.MeshChannelCategory;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Model.Concierge.MeshConciergeCategory;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Model.Concierge.MeshConciergeRequestItem;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Model.Concierge.MeshConciergeRequestService;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Model.Dining.MeshFood;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Model.Dining.MeshFoodCategory;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Model.Message.MeshMessage;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Model.VC.MeshVC;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Model.VC.MeshVCCategory;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Model.VOD.MeshGenre;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Model.VOD.MeshVOD;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Model.Weather.MeshWeatherV2;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.XMPPConnection;

/**
 * Created by ramil on 11/20/17.
 */
@AppSettings(dataSource = AppDataSource.SERVER)
//@AppSettings(dataSource = AppDataSource.RAW)
@RealmEditorSettings()
public class Carlson extends MeshTVApp implements MeshRequestListener,MeshDataListener,ConnectionListener,MeshTVMessageListener {

    public static String TAG=Carlson.class.getSimpleName();

    @Override
    public String getDB() {
        return Carlson.class.getSimpleName();
    }

    @Init(order = 0)
    public void create()
    {
        MeshTVPreferenceManager.updateIPTV(this);
        MeshResourceManager.get().add("ANDROID",R.drawable.and);
        MeshResourceManager.get().add("IOS", R.drawable.ios);
        MeshResourceManager.get().add("WINDOWS",R.drawable.windows);
        MeshResourceManager.get().add("READY_ON",R.drawable.ready_on);
        MeshResourceManager.get().add("READY_OFF",R.drawable.ready_off);

        MeshResourceManager.get().add("e.jpg",R.drawable.channel_e);
        MeshResourceManager.get().add("hbo.jpg",R.drawable.channel_hbo);
        MeshResourceManager.get().add("cbs.jpg",R.drawable.channel_cbs);
        MeshResourceManager.get().add("fox.png",R.drawable.channel_fox);
        MeshResourceManager.get().add("nat_geo.jpg",R.drawable.channel_nat_geo);
        MeshResourceManager.get().add("cnn.png",R.drawable.channel_cnn);
        MeshResourceManager.get().add("abc.jpg",R.drawable.channel_abc);
        MeshResourceManager.get().add("espn.jpg",R.drawable.channel_espn);
        MeshResourceManager.get().add("disney.jpg",R.drawable.channel_disney);


        MeshResourceManager.get().add("vod_hellhiwater",R.drawable.vod_hellhiwater);
        MeshResourceManager.get().add("vod_kingsglaive",R.drawable.vod_kingsglaive);
        MeshResourceManager.get().add("void_secretlife",R.drawable.vod_secretlife);
        MeshResourceManager.get().add("vod_hongkong",R.drawable.vod_hongkong);
        MeshResourceManager.get().add("vod_startrek",R.drawable.vod_startrek);
        MeshResourceManager.get().add("vod_dory",R.drawable.vod_dory);
        MeshResourceManager.get().add("vod_indigation",R.drawable.vod_indigation);
        MeshResourceManager.get().add("vod_alice",R.drawable.vod_alice);
        MeshResourceManager.get().add("vod_mrchurch",R.drawable.vod_mrchurch);
        MeshResourceManager.get().add("vod_imperium",R.drawable.vod_imperium);
        MeshResourceManager.get().add("vod_cap_fantastic",R.drawable.vod_cap_fantastic);
        MeshResourceManager.get().add("vod_lightsout",R.drawable.vod_lightsout);


              /*----------------For Breakfast-------------------------------------*/
        MeshResourceManager.get().add("br_doublemeat.jpg",R.drawable.br_doublemeat);
        MeshResourceManager.get().add("br_freshstart.jpg",R.drawable.br_freshstart);
        MeshResourceManager.get().add("br_oldtimer.jpg",R.drawable.br_oldtimer);
        MeshResourceManager.get().add("br_sunrise.jpg",R.drawable.br_sunrise);
        MeshResourceManager.get().add("br_uncletristan.jpg",R.drawable.br_uncletristan);
        /*----------------For Dinner---------------------------------------*/
        MeshResourceManager.get().add("din_chickenanddumpling.jpg",R.drawable.din_chickenanddumpling);
        MeshResourceManager.get().add("din_chickentender.jpg",R.drawable.din_chickentender);
        MeshResourceManager.get().add("din_harddock.jpg",R.drawable.din_harddock);
        MeshResourceManager.get().add("din_meatloaf.jpg",R.drawable.din_meatloaf);
        MeshResourceManager.get().add("din_roastbeef.jpg",R.drawable.din_roastbeef);
        /*----------------For Lunch---------------------------------------*/
        MeshResourceManager.get().add("lun_friedshrimp.jpg",R.drawable.lun_friedshrimp);
        MeshResourceManager.get().add("lun_lemon.jpg",R.drawable.lun_lemon);
        MeshResourceManager.get().add("lun_rib.jpg",R.drawable.lun_rib);
        MeshResourceManager.get().add("lun_shrimpsteak.jpg",R.drawable.lun_shrimpsteak);
        MeshResourceManager.get().add("lun_sirloin.jpg",R.drawable.lun_sirloin);


        MeshResourceManager.get().add("camara_digital_canon_ixus_145_black_hd_ad_l2.jpg",R.drawable.camara_digital_canon_ixus_145_black_hd_ad_l2);
        MeshResourceManager.get().add("lacoste-5617-573581-5-zoom.jpg",R.drawable.lacoste_5617_573581_5_zoom);
        MeshResourceManager.get().add("empower-6728-278002-1-zoom.jpg",R.drawable.empower_6728_278002_1_zoom);
        MeshResourceManager.get().add("mary-kay-0751-303802-1-zoom.jpg",R.drawable.mary_kay_0751_303802_1_zoom);
        MeshResourceManager.get().add("mary-kay-0939-903802-1-zoom.jpg",R.drawable.mary_kay_0939_903802_1_zoom);
        MeshResourceManager.get().add("underground-3988-895781-1-zoom.jpg",R.drawable.underground_3988_895781_1_zoom);
        MeshResourceManager.get().add("generic-3714-197512-1-zoom.jpg",R.drawable.generic_3714_197512_1_zoom);
        MeshResourceManager.get().add("cover-up-1087-62507-1-zoom.jpg",R.drawable.cover_up_1087_62507_1_zoom);
        MeshResourceManager.get().add("pritech-0999-71479-1-zoom.jpg",R.drawable.pritech_0999_71479_1_zoom);
        MeshResourceManager.get().add("samsung-9236-35639-1-zoom.jpg",R.drawable.samsung_9236_35639_1_zoom);
        MeshResourceManager.get().add("generic-6521-864301-1-zoom.jpg",R.drawable.generic_6521_864301_1_zoom);
        MeshResourceManager.get().add("sennheiser-4399-11276-1-zoom.jpg",R.drawable.sennheiser_4399_11276_1_zoom);
        MeshResourceManager.get().add("rollei-4299-750231-2-zoom.jpg",R.drawable.rollei_4299_750231_2_zoom);
        MeshResourceManager.get().add("juicy-couture-0839-36247-1-zoom.jpg",R.drawable.juicy_couture_0839_36247_1_zoom);
        MeshResourceManager.get().add("sunpak-2400-61947-1-zoom.jpg",R.drawable.sunpak_2400_61947_1_zoom);
        MeshResourceManager.get().add("free-people-4166-131921-1-zoom.jpg",R.drawable.free_people_4166_131921_1_zoom);
        MeshResourceManager.get().add("free-people-4107-421921-1-zoom.jpg",R.drawable.free_people_4107_421921_1_zoom);
        MeshResourceManager.get().add("free-people-4210-248581-1-zoom.jpg",R.drawable.free_people_4210_248581_1_zoom);
        MeshResourceManager.get().add("otto-8987-423291-1-zoom.jpg",R.drawable.otto_8987_423291_1_zoom);
        MeshResourceManager.get().add("longchamp-6569-590022-1-zoom.jpg",R.drawable.longchamp_6569_590022_1_zoom);
        MeshResourceManager.get().add("glamorous-3899-420441-1-zoom.jpg",R.drawable.glamorous_3899_420441_1_zoom);
        MeshResourceManager.get().add("xanina-3987-644962-1-zoom.jpg",R.drawable.xanina_3987_644962_1_zoom);
        MeshResourceManager.get().add("the-fille-9029-969842-1-zoom.jpg",R.drawable.the_fille_9029_969842_1_zoom);
        MeshResourceManager.get().add("love-my-clothes-0090-230842-1-zoom.jpg",R.drawable.love_my_clothes_0090_230842_1_zoom);
        MeshResourceManager.get().add("rock-religion-0945-227591-1-zoom.jpg",R.drawable.rock_religion_0945_227591_1_zoom);
        MeshResourceManager.get().add("love-my-clothes-7675-785052-2-zoom.jpg",R.drawable.love_my_clothes_7675_785052_2_zoom);
        MeshResourceManager.get().add("dead-lovers-4948-846591-1-zoom.jpg",R.drawable.dead_lovers_4948_846591_1_zoom);
        MeshResourceManager.get().add("weiying-4131-107962-1-zoom.jpg",R.drawable.weiying_4131_107962_1_zoom);
        MeshResourceManager.get().add("ouliud-9227-679162-1-zoom.jpg",R.drawable.ouliud_9227_679162_1_zoom);
        MeshResourceManager.get().add("kipling-9333-222842-1-zoom.jpg",R.drawable.kipling_9333_222842_1_zoom);
        MeshResourceManager.get().add("41oer8upAiL.jpg",R.drawable.oer);
        MeshResourceManager.get().add("71qeVO9W19L._SL1500__.jpg",R.drawable.qe);
        MeshResourceManager.get().add("51tOoq6PbNL.jpg",R.drawable.to);
        MeshResourceManager.get().add("61SZqvIFM2L._SL1001__.jpg",R.drawable.sz);
        MeshResourceManager.get().add("61KFMcGkWrL._SL1001__.jpg",R.drawable.kmf);
        MeshResourceManager.get().add("71Er5AY778L._SL1001__.jpg",R.drawable.er);
        MeshResourceManager.get().add("71kdz3i2jsL._SL1001__.jpg",R.drawable.kdz);
        MeshResourceManager.get().add("41aAC8kFFlL._SY450__.jpg",R.drawable.aa);
        MeshResourceManager.get().add("519J6IVnCHL._SL1350__.jpg",R.drawable.j6);
        MeshResourceManager.get().add("51LtniXYBCL.jpg",R.drawable.ltnix);

        MeshResourceManager.get().add("T2eC16RHJGMFFpcQV,r(BR)nTOvjLg~~60_57.JPG",R.drawable.t1);
        MeshResourceManager.get().add("T2eC16N,!zQE9s3srYizBRtB2k,s2!~~60_12.JPG",R.drawable.t2e);
        MeshResourceManager.get().add("generic-1350-150642-1-zoom.jpg",R.drawable.generic_1350_150642_1_zoom);
        MeshResourceManager.get().add("penshoppe-1440-778712-1-zoom.jpg",R.drawable.penshoppe_1440_778712_1_zoom);
        MeshResourceManager.get().add("tnl-5152-133802-1-zoom.jpg",R.drawable.tnl_5152_133802_1_zoom);
        MeshResourceManager.get().add("santa-barbara-polo-racquet-club-7900-815102-1-zoom.jpg",R.drawable.santa_barbara_polo_racquet_club_7900_815102_1_zoom);

        MeshResourceManager.get().add("tnl-2767-463802-1-zoom.jpg",R.drawable.tnl_2767_463802_1_zoom);
        MeshResourceManager.get().add("geneva-5617-740211-3-zoom.jpg",R.drawable.geneva_5617_740211_3_zoom);
        MeshResourceManager.get().add("wagw-5617-717921-5-zoom.jpg",R.drawable.wagw_5617_717921_5_zoom);
        MeshResourceManager.get().add("wagw-5617-274731-3-zoom.jpg",R.drawable.wagw_5617_274731_3_zoom);
        MeshResourceManager.get().add("generic-0276-329232-1-zoom.jpg",R.drawable.generic_0276_329232_1_zoom);
        MeshResourceManager.get().add("hickok-0859-319632-1-zoom.jpg",R.drawable.hickok_0859_319632_1_zoom);
        MeshResourceManager.get().add("jewelmine-3276-948752-1-zoom.jpg",R.drawable.jewelmine_3276_948752_1_zoom);
        MeshResourceManager.get().add("jewelmine-5087-919752-1-zoom.jpg",R.drawable.jewelmine_5087_919752_1_zoom);
        MeshResourceManager.get().add("penshoppe-7268-928842-1-zoom.jpg",R.drawable.penshoppe_7268_928842_1_zoom);
        MeshResourceManager.get().add("jewelmine-3722-551852-1-zoom.jpg",R.drawable.jewelmine_3722_551852_1_zoom);
        MeshResourceManager.get().add("hickok-8533-409632-2-zoom.jpg",R.drawable.hickok_8533_409632_2_zoom);

        MeshResourceManager.get().add("camara_digital_canon_ixus_145_black_hd_ad_l2", R.drawable.camara_digital_canon_ixus_145_black_hd_ad_l2);


        MeshResourceManager.get().add("intro1",R.drawable.intro1);
        MeshResourceManager.get().add("intro2",R.drawable.intro2);
        MeshResourceManager.get().add("intro3",R.drawable.intro3);
        MeshResourceManager.get().add("intro4",R.drawable.intro4);
        MeshResourceManager.get().add("intro5",R.drawable.intro5);
        MeshResourceManager.get().add("intro6",R.drawable.intro6);
        MeshResourceManager.get().add("intro7",R.drawable.intro7);

        MeshResourceManager.get().add("quorvus1",R.drawable.quorvus1);
        MeshResourceManager.get().add("quorvus2",R.drawable.quorvus2);
        MeshResourceManager.get().add("quorvus3",R.drawable.quorvus3);

        MeshResourceManager.get().add("radissonblu1",R.drawable.radissonblu1);
        MeshResourceManager.get().add("radissonblu2",R.drawable.radissonblu2);
        MeshResourceManager.get().add("radissonblu3",R.drawable.radissonblu3);
        MeshResourceManager.get().add("radissonblu4",R.drawable.radissonblu4);

        MeshResourceManager.get().add("radisson1",R.drawable.radisson1);
        MeshResourceManager.get().add("radisson2",R.drawable.radisson2);
        MeshResourceManager.get().add("radisson3",R.drawable.radisson3);
        MeshResourceManager.get().add("radisson4",R.drawable.radisson4);

        MeshResourceManager.get().add("radissonred1",R.drawable.radissonred1);
        MeshResourceManager.get().add("radissonred2",R.drawable.radissonred2);
        MeshResourceManager.get().add("radissonred3",R.drawable.radissonred3);
        MeshResourceManager.get().add("radissonred4",R.drawable.radissonred4);

        MeshResourceManager.get().add("park1",R.drawable.park1);
        MeshResourceManager.get().add("park2",R.drawable.park2);
        MeshResourceManager.get().add("park3",R.drawable.park3);
        MeshResourceManager.get().add("park4",R.drawable.park4);

        MeshResourceManager.get().add("parkinn1",R.drawable.parkinn1);
        MeshResourceManager.get().add("parkinn2",R.drawable.parkinn2);
        MeshResourceManager.get().add("parkinn3",R.drawable.parkinn3);
        MeshResourceManager.get().add("parkinn4",R.drawable.parkinn4);

        MeshResourceManager.get().add("countryinn1",R.drawable.countryinn1);
        MeshResourceManager.get().add("countryinn2",R.drawable.countryinn2);
        MeshResourceManager.get().add("countryinn3",R.drawable.countryinn3);
        MeshResourceManager.get().add("countryinn4",R.drawable.countryinn4);

        MeshResourceManager.get().add("logo_lookup",R.drawable.logo_lockup);
        MeshResourceManager.get().add("logo__quorvus",R.drawable.logo__quorvus);
        MeshResourceManager.get().add("logo__radissonblu",R.drawable.logo__radissonblu);
        MeshResourceManager.get().add("logo__radisson",R.drawable.logo__radisson);
        MeshResourceManager.get().add("logo__radissonred",R.drawable.logo__radissonred);
        MeshResourceManager.get().add("logo__parkplaza",R.drawable.logo__parkplaza);
        MeshResourceManager.get().add("logo__parkinn",R.drawable.logo__park);
        MeshResourceManager.get().add("logo__countryinn",R.drawable.logo__countryinn);


    }

    @Init(order = 2)
    public void register()
    {
        if(getDataSourceSetting()== AppDataSource.SERVER)
        {
            new MeshRegisterTask(this,this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    @Init(order = 3)
    public void connectXMPP()
    {
        if(getDataSourceSetting()==AppDataSource.SERVER)
        {
            MeshXMPPManager.setListener(this);
            new XMPPConnectTask(this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,null);
        }
    }

    @Init(order = 4)
    public void getGuest()
    {
        if(getDataSourceSetting()==AppDataSource.SERVER)
        {
            MeshTVDataManager.requestData(MeshGuest.class,this);
            MeshTVDataManager.requestData(MeshConfig.class,this);
        }
    }

    @Init(order = 5)
    public void reportAnalytics()
    {
        if(getDataSourceSetting()==AppDataSource.SERVER)
        {
            new ReportAnalytics(getApplicationContext(), new MeshRequestListener() {
                @Override
                public void onFailed(String s) {

                }

                @Override
                public void onResult(String s) {

                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,null);
        }
    }
    @Init(order = 6)
    public void updateMessages()
    {
        if(getDataSourceSetting()==AppDataSource.SERVER)
        {
            MeshTVDataManager.requestData(MeshMessage.class);
        }
    }
    @Init(order = 8)
    public void updateWeather()
    {
        if(getDataSourceSetting()==AppDataSource.SERVER)
        {
            MeshTVDataManager.requestData(MeshWeatherV2.class);
            MeshTVDataManager.requestData(MeshWeatherLocal.class,this);
        }
    }
    @Init(order = 9)
    public void updateData()
    {
        if(getDataSourceSetting()==AppDataSource.SERVER)
        {
            MeshTVDataManager.requestData(MeshChannel.class);
            MeshTVDataManager.requestData(MeshChannelCategory.class);
            MeshTVDataManager.requestData(MeshFood.class);
            MeshTVDataManager.requestData(MeshFoodCategory.class);
            MeshTVDataManager.requestData(MeshVOD.class);
            MeshTVDataManager.requestData(MeshGenre.class);
            MeshTVDataManager.requestData(MeshConciergeRequestService.class);
            MeshTVDataManager.requestData(MeshConciergeRequestItem.class);
            MeshTVDataManager.requestData(MeshConciergeCategory.class);
            MeshTVDataManager.requestData(MeshVC.class);
            MeshTVDataManager.requestData(MeshVCCategory.class);
            MeshTVDataManager.requestData(MeshBillV2.class);
        }
    }

    //==============================================================================================
    //===========================================MeshRequestListener================================
    @Override
    public void onFailed(String s) {

    }
    @Override
    public void onResult(String s) {

    }
    //==============================================================================================
    //============================================MeshDataListener==================================
    @Override
    public void onNoNetwork(Class aClass) {
    }
    @Override
    public void onNoData(Class aClass) {

    }
    @Override
    public void onParseFailed(Class aClass, String s) {

    }
    @Override
    public void onFileNotFound(Class aClass, String s) {

    }
    @Override
    public void onDataReceived(Class aClass, int i) {

    }
    //==============================================================================================

    //===========================================XMPP===============================================
    @Override
    public void connected(XMPPConnection connection) {

    }

    @Override
    public void authenticated(XMPPConnection connection, boolean resumed) {

    }

    @Override
    public void connectionClosed() {

    }

    @Override
    public void connectionClosedOnError(Exception e) {

    }

    @Override
    public void reconnectionSuccessful() {

    }

    @Override
    public void reconnectingIn(int seconds) {

    }

    @Override
    public void reconnectionFailed(Exception e) {

    }

    //==============================================================================================
    //==========================================XMPP Connection Task================================
    public class XMPPConnectTask extends  AsyncTask<Void,Void,Void>
    {

        ConnectionListener listener;

        public XMPPConnectTask(ConnectionListener listener)
        {
            this.listener = listener;
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            if(getDataSourceSetting()==AppDataSource.SERVER)
            {
                MeshXMPPPreference.enablePLAIN(getApplicationContext(),true);
                MeshXMPPPreference.enableDIGESTMD5(getApplicationContext(),true);
                MeshXMPPPreference.enableSCRAMSHA1(getApplicationContext(),true);
                MeshXMPPPreference.setShouldReport(getApplicationContext(),true);
                MeshXMPPPreference.setReportTo(getApplicationContext(),"mars2");
                MeshXMPPUser user =
                        new MeshXMPPUser(
                                MeshTVPreferenceManager.getXUsername(getApplicationContext()),

                                MeshTVPreferenceManager.getPassword(getApplicationContext()),
                                true,
                                "mars2"
                        );
                user.setDomain("localhost");
                user.setHost(MeshTVPreferenceManager.getXMPPHost(getApplicationContext()));
                user.setPort(MeshTVPreferenceManager.getXMPPPort(getApplicationContext()));
                MeshXMPPManager.connect(getApplicationContext(),listener,user);
            }
            return null;
        }
    }
    //==============================================================================================
    //==========================================MeshMessageListener=================================
    @Override
    public void receiveMessage(String s, String s1)
    {
        Log.i(TAG+"-DATA",s+":"+s1);
        MeshData data =new MeshData(s1);

        Log.i(TAG+"-DATA",data.getData()+"");
        if(MeshXMPPPreference.shouldReport(getApplicationContext()))
        {
            MeshXMPPManager.respond(s,s1);
        }
        data.updateData();
    }
    //==============================================================================================

}
