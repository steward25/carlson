package com.ph.bittelasia.meshtv.tv.carlson.Movies.View.Fragment;

import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ph.bittelasia.meshtv.tv.carlson.R;
import com.ph.bittelasia.meshtv.tv.carlson.Temp.View.Dialog.CarlsonConfirm;
import com.ph.bittelasia.meshtv.tv.carlson.Temp.View.Dialog.MeshTVDialog;
import com.ph.bittelasia.meshtv.tv.carlson.Temp.View.Fragment.CarlsonPreview;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Listener.MeshTVFragmentListener;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Model.VOD.MeshVOD;

import java.text.DecimalFormat;

/**
 * Created by ramil on 2/8/18.
 */

public class CarlsonVodPreview extends MeshTVDialog implements MeshTVFragmentListener{

    double percentageWidth;
    double percentageHeight;

    CarlsonPreview preview;

    FrameLayout  ll_preview;
    TextView     tv_title;
    TextView     tv_price;
    TextView     tv_desc;
    Button       btn_rent;

    MeshVOD vod;

    public static CarlsonVodPreview d;

    public static CarlsonVodPreview dialog(MeshVOD vod,double percentageWidth,double percentageHeight)
    {
        d=new CarlsonVodPreview();
        d.vod=vod;
        d.percentageWidth=percentageWidth;
        d.percentageHeight=percentageHeight;   //MeshTVDemoFileReader.getMediaPath();

        return d;
    }

    @Override
    public void setIDs(View view) {
        ll_preview=(FrameLayout) view.findViewById(R.id.ll_preview);
        tv_title=(TextView)view.findViewById(R.id.tv_title);
        tv_price=(TextView)view.findViewById(R.id.tv_price);
        tv_desc=(TextView)view.findViewById(R.id.tv_desc);
        btn_rent=(Button)view.findViewById(R.id.btn_rent);
    }

    @Override
    public void setContent() {
        try {
            tv_title.setText(vod.getTitle());
            DecimalFormat df = new DecimalFormat("$" + " ###,##0.00");
            tv_price.setText(df.format(vod.getPrice()));
            tv_desc.setText(Html.fromHtml(vod.getPlot()));
            btn_rent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CarlsonConfirm.dialog(vod,.35,.30).show(getChildFragmentManager(),"confirm");
                }
            });
            preview = CarlsonPreview.preview(vod, 0);
            getChildFragmentManager().beginTransaction().add(R.id.ll_preview, preview, "container").commit();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public int setLayout() {
        return R.layout.carlson_vod_preview_dialog;
    }

    @Override
    public int setConstraintLayout() {
            return R.id.cl_layout;
    }

    @Override
    public double setPercentageWidth() {
        return percentageWidth;
    }

    @Override
    public double setPercentageHeight() {
        return percentageHeight;
    }

    @Override
    public void onClicked(Object o) {
        if(o instanceof String )
        {
            if(((String)o).equals(getActivity().getResources().getString(R.string.yes)))
            {
                getDialog().dismiss();
            }
        }
    }

    @Override
    public void onSelected(Object o) {

    }
}
