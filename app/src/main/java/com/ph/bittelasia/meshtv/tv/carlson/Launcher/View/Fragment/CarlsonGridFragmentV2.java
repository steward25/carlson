package com.ph.bittelasia.meshtv.tv.carlson.Launcher.View.Fragment;

import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.ph.bittelasia.meshtv.tv.carlson.R;
import com.ph.bittelasia.meshtv.tv.carlson.Temp.Model.Packages;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Annotation.View.General.Layout;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Annotation.View.Widget.BindWidget;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.View.Fragment.MeshTVFragment;

import java.util.ArrayList;

/**
 * Created by ramil on 2/6/18.
 */
@Layout(R.layout.carlson_launcher_gridv2)
public class CarlsonGridFragmentV2 extends MeshTVFragment {

    Animation animZoomIn;

    @BindWidget(R.id.rl_airmedia)
    public RelativeLayout airmedia;
    @BindWidget(R.id.rl_message)
    public RelativeLayout message;
    @BindWidget(R.id.rl_weather)
    public RelativeLayout weather;
    @BindWidget(R.id.rl_hotelinfo)
    public RelativeLayout hotelinfo;
    @BindWidget(R.id.rl_bill)
    public RelativeLayout bill;
    @BindWidget(R.id.rl_shopping)
    public RelativeLayout shopping;
    @BindWidget(R.id.rl_dining)
    public RelativeLayout dining;
    @BindWidget(R.id.rl_vod)
    public RelativeLayout vod;
    @BindWidget(R.id.rl_tv)
    public RelativeLayout tv;

    public void setEvents() {
        try
        {
            animZoomIn = AnimationUtils.loadAnimation(getContext(), R.anim.zoom_in);
            airmedia.setOnFocusChangeListener(focusChangeListener);
            message.setOnFocusChangeListener(focusChangeListener);
            weather.setOnFocusChangeListener(focusChangeListener);
            hotelinfo.setOnFocusChangeListener(focusChangeListener);
            bill.setOnFocusChangeListener(focusChangeListener);
            shopping.setOnFocusChangeListener(focusChangeListener);
            dining.setOnFocusChangeListener(focusChangeListener);
            vod.setOnFocusChangeListener(focusChangeListener);
            tv.setOnFocusChangeListener(focusChangeListener);

            airmedia.setOnHoverListener(hover);
            message.setOnHoverListener(hover);
            weather.setOnHoverListener(hover);
            hotelinfo.setOnHoverListener(hover);
            bill.setOnHoverListener(hover);
            shopping.setOnHoverListener(hover);
            dining.setOnHoverListener(hover);
            vod.setOnHoverListener(hover);
            tv.setOnHoverListener(hover);

            airmedia.setOnTouchListener(touchListener);
            message.setOnTouchListener(touchListener);
            weather.setOnTouchListener(touchListener);
            hotelinfo.setOnTouchListener(touchListener);
            bill.setOnTouchListener(touchListener);
            shopping.setOnTouchListener(touchListener);
            dining.setOnTouchListener(touchListener);
            vod.setOnTouchListener(touchListener);
            tv.setOnTouchListener(touchListener);

            airmedia.setOnClickListener(clickListener);
            message.setOnClickListener(clickListener);
            weather.setOnClickListener(clickListener);
            hotelinfo.setOnClickListener(clickListener);
            bill.setOnClickListener(clickListener);
            shopping.setOnClickListener(clickListener);
            dining.setOnClickListener(clickListener);
            vod.setOnClickListener(clickListener);
            tv.setOnClickListener(clickListener);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public void checkImage(final View view, Boolean b, Animation animation)
    {
        View first_child = ((ViewGroup)view).getChildAt(1);
        View second_child = ((ViewGroup)view).getChildAt(2);
        View third_child = ((ViewGroup)view).getChildAt(3);
        if(b)
        {

            view.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.carlson_image_item_selectedv3));
            view.startAnimation(animation);
            first_child.setForeground(ContextCompat.getDrawable(getContext(),R.drawable.carlson_image_item_selectedv1));
            second_child.setTranslationZ(2);
            third_child.setTranslationZ(3);
            view.setTranslationZ(1);
        }
        else
        {
            view.setBackground(null);
            view.setPadding(0,0,0,0 );
            view.setTranslationZ(0);
            first_child.setForeground(null);
            second_child.setBackgroundTintMode(null);
            second_child.setTranslationZ(0);
            third_child.setTranslationZ(0);
            view.clearAnimation();
        }

    }

    final  View.OnFocusChangeListener focusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean b) {
            checkImage(view,b,animZoomIn);
        }
    };

    final View.OnHoverListener hover=new View.OnHoverListener() {
        @Override
        public boolean onHover(View view, MotionEvent motionEvent) {
            view.requestFocus();
            return false;
        }
    };

    final View.OnTouchListener touchListener=new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                view.performClick();
            } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                view.requestFocus();
            }
            return false;
        }
    };

    //----------------global onClicklistener-----------------------------------------------------

    final View.OnClickListener clickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int id=view.getId();
            switch (id)
            {
                case R.id.rl_airmedia:
                    getListener().onClicked(Packages.DISPLAY_AIRMEDIA);
                    break;
                case R.id.rl_message:
                    getListener().onClicked(Packages.DISPLAY_MESSAGE);
                    break;
                case R.id.rl_hotelinfo:
                    getListener().onClicked(Packages.DISPLAY_FACILITY);
                    break;
                case R.id.rl_weather:
                    getListener().onClicked(Packages.DISPLAY_WEATHER);
                    break;
                case R.id.rl_bill:
                    getListener().onClicked(Packages.DISPLAY_BILL);
                    break;
                case R.id.rl_shopping:
                    getListener().onClicked(Packages.DISPLAY_SHOPPING);
                    break;
                case R.id.rl_dining:
                    getListener().onClicked(Packages.DISPLAY_DINING);
                    break;
                case R.id.rl_vod:
                    getListener().onClicked(Packages.DISPLAY_VOD);
                    break;
                case R.id.rl_tv:
                    getListener().onClicked(Packages.DISPLAY_TV);
                    break;
                default:
                    break;
            }

        }
    };


    @Override
    protected void onDrawDone(View view) {
        setEvents();
    }

    @Override
    protected void onDataUpdated(ArrayList arrayList) {

    }

    @Override
    protected void onNewData(Object o) {

    }

    @Override
    protected void onDataUpdated(Object o, int i) {

    }

    @Override
    protected void onDeleteData(int i) {

    }

    @Override
    protected void onClearData() {

    }

    @Override
    protected void onDataNotFound(Class aClass) {

    }

    @Override
    protected void refresh() {

    }

    @Override
    protected void update(Object o) {

    }
}
