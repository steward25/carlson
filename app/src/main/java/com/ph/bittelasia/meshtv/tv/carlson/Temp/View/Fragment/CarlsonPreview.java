package com.ph.bittelasia.meshtv.tv.carlson.Temp.View.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.ph.bittelasia.meshtv.tv.carlson.R;
import com.ph.bittelasia.meshtv.tv.carlson.Temp.Model.PreviewSession;
import com.ph.bittelasia.meshtv.tv.carlson.Temp.Model.SessionManager;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Annotation.View.Exoplayer.MeshTVPlayerSettings;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Constant.AppDataSource;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Util.FileReader.MeshTVDemoFileReader;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.View.App.MeshTVApp;
import com.ph.bittelasia.meshtv.tv.mtvlib.ExoPlayer.View.Fragment.MeshTVExoplayerFragment;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Model.Channel.MeshChannel;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Model.VOD.MeshVOD;

/**
 * Created by ramil on 2/8/18.
 */
@MeshTVPlayerSettings(layout = R.layout.carlson_player, exoplayer = R.id.exo_prev,hasControls=true)
public class CarlsonPreview extends MeshTVExoplayerFragment
{

    /* 0 = trailer, 1 = full */

    public static CarlsonPreview fragment;
    public SimpleExoPlayerView sv;

    LinearLayout   ll_press;
    TextView       tv_insruct;
    PreviewSession previewSession;
    int full;
    MeshVOD     meshVod;
    MeshChannel meshChannel;
    Object object;

    public static CarlsonPreview preview(Object object,int full)
    {
        fragment=new CarlsonPreview();
        fragment.object=object;
        fragment.full=full;
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        sv = (SimpleExoPlayerView) view.findViewById(R.id.exo_prev);
        sv.getVideoSurfaceView().bringToFront();
        ll_press=(LinearLayout)view.findViewById(R.id.ll_press);
        tv_insruct=(TextView)view.findViewById(R.id.tv_instruct);
        play();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        play();
    }

    public void play()
    {
        try
        {
            previewSession = new PreviewSession();
            if (object instanceof MeshVOD) {
                sv.setUseController(true);
                meshVod = (MeshVOD) object;
                if (full == 0) {
                    if(MeshTVApp.get().getDataSourceSetting()== AppDataSource.SERVER)
                    {
                        setObjectURL(meshVod.getTrailer());
                        sv.getPlayer().setPlayWhenReady(true);
                        sv.getPlayer().setRepeatMode(Player.REPEAT_MODE_ONE);
                        ll_press.setVisibility(View.GONE);
                        Log.i("steward","trailer: server->: "+meshVod.getTrailer());
                    }
                    else
                    {
                        setObjectURL(MeshTVDemoFileReader.getMediaPath() + meshVod.getTrailer());
                        ll_press.setVisibility(View.VISIBLE);
                        Log.i("steward", "vod preview: " + MeshTVDemoFileReader.getMediaPath() + meshVod.getTrailer());
                    }
                } else {
                    if(MeshTVApp.get().getDataSourceSetting()== AppDataSource.SERVER)
                    {
                        setObjectURL(meshVod.getFull());
                        Log.i("steward","full: server->: "+meshVod.getFull());
                    }
                    else
                    {
                        setObjectURL(MeshTVDemoFileReader.getMediaPath() + meshVod.getFull());
                        Log.i("steward", "vod preview: " + MeshTVDemoFileReader.getMediaPath() + meshVod.getFull());
                        sv.getPlayer().seekTo(previewSession.vodList.get(meshVod.getId()));
                    }
                }
                tv_insruct.setText(getActivity().getResources().getString(R.string.vod_instruct));
            } else if (object instanceof MeshChannel) {
                meshChannel = (MeshChannel) object;
                sv.setUseController(false);
                Log.i("steward", "channel preview: " + MeshTVDemoFileReader.getMediaPath() + meshChannel.getChannel_uri());
                if(MeshTVApp.get().getDataSourceSetting()== AppDataSource.SERVER)
                {
                    setObjectURL(meshChannel.getChannel_uri());
                }
                else
                {
                    setObjectURL(MeshTVDemoFileReader.getMediaPath() + meshChannel.getChannel_uri());
                    sv.getPlayer().seekTo(previewSession.vodList.get(meshChannel.getId()));
                }
                tv_insruct.setText(getActivity().getResources().getString(R.string.tv_instruct));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void updateChannel(MeshChannel channel)
    {
        sv.getPlayer().stop();
        setObjectURL(channel.getChannel_uri());
        sv.getPlayer().setPlayWhenReady(true);
    }

    public void getPosition()
    {
        try {
            if (sv.getPlayer().getPlayWhenReady()) {
                if(object instanceof MeshVOD)
                {
                    previewSession=new  PreviewSession(meshVod.getId(),sv.getPlayer().getCurrentPosition(),meshVod);
                    previewSession.vodList.put(meshVod.getId(),sv.getPlayer().getCurrentPosition());
                    new SessionManager(getContext()).setKeyChange(sv.getPlayer().getCurrentPosition(), meshVod.getId());
                }
                if(object instanceof MeshChannel)
                {
                    previewSession=new  PreviewSession(meshVod.getId(),sv.getPlayer().getCurrentPosition(),meshChannel);
                    previewSession.vodList.put(meshChannel.getId(),sv.getPlayer().getCurrentPosition());
                    new SessionManager(getContext()).setKeyChange(sv.getPlayer().getCurrentPosition(), meshChannel.getId());
                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
//        try
//        {
//            handler.removeCallbacks(r);
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
    }

    public void setChannel(MeshVOD meshVod)
    {
//        try
//        {
//            handler.removeCallbacks(r);
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//        this.meshVod = meshVod;
//        handler.postDelayed(r,20000);;
    }
}
