package com.ph.bittelasia.meshtv.tv.carlson.Dining.Model;


import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.ph.bittelasia.meshtv.tv.carlson.R;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Annotation.View.ViewHolder.ViewHolderLayout;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Util.ResourceManager.MeshResourceManager;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Object.Adapter.MeshTVAdapter;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Object.VH.MeshTVVHolder;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Model.Dining.MeshFood;


import java.util.ArrayList;

/**
 * Created by ramil on 1/23/18.
 */
@ViewHolderLayout(layout = R.layout.carlson_grid_list_item)
public class CarlsonDiningListAdapter extends MeshTVAdapter<MeshFood> {

    private VelocityTracker mVelocityTracker = null;


    public CarlsonDiningListAdapter(Context context, GridView gv_view, int layoutResourceId, ArrayList<MeshFood> data) {
        super(context, gv_view, layoutResourceId, data);
    }

    @Override
    public MeshTVVHolder setViewHolder() {
        return new CarlsonDiningListViewHolder();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public void updateRow(MeshTVVHolder meshTVVHolder, final MeshFood meshFood) {
        CarlsonDiningListViewHolder vh=(CarlsonDiningListViewHolder)meshTVVHolder;
        try
        {
//            Log.i("steward",meshFood.getImg_uri());
//            vh.getIv_icon().setImageDrawable(getContext().getResources().getDrawable(MeshResourceManager.get().getResourceId(meshFood.getImg_uri())));
            MeshResourceManager.displayLiveImageFor(getContext(),vh.getIv_icon(),meshFood.getImg_uri());
            vh.getTv_name().setText(meshFood.getItem_name());
//            ViewGroup vg=(ViewGroup) vh.getV();
//            vg.setOnHoverListener(new View.OnHoverListener() {
//                @Override
//                public boolean onHover(View view, MotionEvent motionEvent) {
//                    int index = motionEvent.getActionIndex();
//                    int action = motionEvent.getActionMasked();
//                    int pointerId = motionEvent.getPointerId(index);
//                    switch(action) {
//                        case MotionEvent.ACTION_HOVER_ENTER:
//                            if(mVelocityTracker == null) {
//                                mVelocityTracker = VelocityTracker.obtain();
//                            }
//                            else {
//                                mVelocityTracker.clear();
//                            }
//                            getGv_view().setSelection(0);
//                            mVelocityTracker.addMovement(motionEvent);
//                            return true;
//                        case MotionEvent.ACTION_HOVER_MOVE:
//                            mVelocityTracker.addMovement(motionEvent);
//                            mVelocityTracker.computeCurrentVelocity(1000);
//                          //  Log.i("steward","postion"+getPosition(meshFood));
//                            getGv_view().setSelection(getPosition(meshFood));
//                            view.setHovered(true);
//                            view.setSelected(true);
//                            return true;
//                        case MotionEvent.ACTION_HOVER_EXIT:
//                            view.setHovered(false);
//                            view.setSelected(false);
//                            notifyDataSetChanged();
//                            return true;
//                        case MotionEvent.ACTION_UP:
//                            return true;
//                        case MotionEvent.ACTION_CANCEL:
//                            mVelocityTracker.recycle();
//                            return true;
//                    }
//                    return false;
//                }
//            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
