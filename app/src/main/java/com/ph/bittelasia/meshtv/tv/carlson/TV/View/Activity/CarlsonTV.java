package com.ph.bittelasia.meshtv.tv.carlson.TV.View.Activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.MotionEvent;

import com.ph.bittelasia.meshtv.tv.carlson.Core.View.Activity.CarlsonActivity;
import com.ph.bittelasia.meshtv.tv.carlson.R;
import com.ph.bittelasia.meshtv.tv.carlson.TV.View.Fragment.CarlsonTVCategory;
import com.ph.bittelasia.meshtv.tv.carlson.TV.View.Fragment.CarlsonTVList;
import com.ph.bittelasia.meshtv.tv.carlson.Temp.Model.MeshArrayListCallBack;
import com.ph.bittelasia.meshtv.tv.carlson.Temp.View.Activity.CarlsonFullScreen;
import com.ph.bittelasia.meshtv.tv.carlson.Temp.View.Fragment.CarlsonPreview;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Annotation.View.Activity.ActivitySetting;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Annotation.View.Activity.AttachFragment;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Annotation.View.General.Layout;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Listener.MeshTVFragmentListener;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.View.Activity.MeshTVActivity;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Model.Channel.MeshChannel;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Model.Channel.MeshChannelCategory;

import java.security.spec.ECField;
import java.util.ArrayList;

/**
 * Created by ramil on 2/7/18.
 */
@Layout(R.layout.carlson_tv_layout)
@ActivitySetting()
public class CarlsonTV extends CarlsonActivity implements MeshTVFragmentListener,MeshArrayListCallBack<MeshChannel> {

    CarlsonTVCategory category;
    CarlsonTVList     list;
    CarlsonPreview    preview;
    Object            object;

    private int _xDelta;
    private int _yDelta;
    boolean     loaded=false;

    MeshChannelCategory meshCategory;

    @Override
    public void onResume() {
        super.onResume();
        loaded=false;
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_UP:
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_MOVE:
                break;
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void onClicked(Object o) {
        if(o instanceof MeshChannel)
        {
            Intent i = new Intent(this, CarlsonFullScreen.class);
            i.putExtra("CHANNEL", ((MeshChannel)o).getId());
            i.putExtra("MEDIA","CHANNEL");
            this.object=o;
            startActivityForResult(i,100);
            Log.i("steward","channel: "+((MeshChannel)o).getChannel_uri());
        }
        if (o instanceof MeshChannelCategory)
        {
            meshCategory = (MeshChannelCategory) o;
            list.setCategory(meshCategory.getId());
        }
    }

    @Override
    public void onSelected(Object o) {
        try
        {
            if (!loaded)
            {
                if (o instanceof MeshChannelCategory)
                {
                    meshCategory = (MeshChannelCategory) o;
                    list.setCategory(meshCategory.getId());
                    loaded = true;
                    Log.i("steward", "channel notified ok");
                }
            }
            else
            {
                list.adapter.notifyDataSetChanged();
                list.gv_grid.invalidateViews();
                Log.i("steward", "channel notified");
            }
            if (o instanceof MeshChannel)
            {
                if (preview == null)
                {
                    preview = CarlsonPreview.preview(o, 0);
                    getSupportFragmentManager().beginTransaction().add(R.id.ll_display, preview, "display").commit();
                }
                else
                {
                    preview = CarlsonPreview.preview(o, 0);
                    getSupportFragmentManager().beginTransaction().replace(R.id.ll_display, preview, "display").commit();
                }
                Log.i("steward", "channel: " + ((MeshChannel) o).getChannel_uri());
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode)
        {
            case 100:
                switch (resultCode)
                {
                    case RESULT_OK:
                        preview=null;
                        preview = CarlsonPreview.preview(this.object, 0);
                        getSupportFragmentManager().beginTransaction().replace(R.id.ll_display, preview, "display").commit();
                        break;
                }
        }
    }

    @AttachFragment(container = R.id.ll_categories,tag="categories",order = 3)
    public Fragment attachCategory()
    {
        category=new CarlsonTVCategory();
        return  category;
    }

    @AttachFragment(container = R.id.ll_list,tag="list",order = 4)
    public Fragment attachList()
    {
        list=new CarlsonTVList();
        return  list;
    }

    @Override
    public void meshArrayList(ArrayList<MeshChannel> list) {

    }
}
