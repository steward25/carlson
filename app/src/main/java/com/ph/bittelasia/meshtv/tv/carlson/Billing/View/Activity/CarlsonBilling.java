package com.ph.bittelasia.meshtv.tv.carlson.Billing.View.Activity;

import android.support.v4.app.Fragment;
import android.view.KeyEvent;


import com.ph.bittelasia.meshtv.tv.carlson.Billing.View.Fragment.CarlsonBillingList;
import com.ph.bittelasia.meshtv.tv.carlson.Billing.View.Fragment.CarlsonBillingTotal;
import com.ph.bittelasia.meshtv.tv.carlson.Core.View.Activity.CarlsonActivity;
import com.ph.bittelasia.meshtv.tv.carlson.R;
import com.ph.bittelasia.meshtv.tv.carlson.Temp.Model.MeshTVButtons;
import com.ph.bittelasia.meshtv.tv.carlson.Temp.View.Dialog.CarlsonConfirm;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Annotation.View.Activity.ActivitySetting;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Annotation.View.Activity.AttachFragment;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Annotation.View.General.Layout;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Listener.MeshTVFragmentListener;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Util.RemoteControl.KR301KeyCode;

/**
 * Created by ramil on 1/11/18.
 */
@Layout(R.layout.carlson_checkout)
@ActivitySetting()
public class CarlsonBilling extends CarlsonActivity implements MeshTVFragmentListener {

    CarlsonBillingList list;
    CarlsonBillingTotal billingTotal;

    @Override
    public boolean dispatchKeyEvent(KeyEvent event)
    {
        switch (event.getAction())
        {
            case KeyEvent.ACTION_UP:
                switch (KR301KeyCode.getEquivalent(event.getKeyCode()))
                {

                    case 9:
                        if(event.getAction()==KeyEvent.ACTION_UP)
                        {
                            return true;
                        }
                }
                break;
        }

        return super.dispatchKeyEvent(event);
    }




    @AttachFragment(container = R.id.ll_display,tag = "list",order = 2)
    public Fragment attachList()
    {
        list=CarlsonBillingList.get(billingTotal);
        return list;
    }

    @AttachFragment(container = R.id.ll_total,tag="total",order=1)
    public Fragment attachTotal()
    {
        billingTotal=new CarlsonBillingTotal();
        return  billingTotal;
    }


    @Override
    public void onClicked(Object o) {
        try
        {
            if (o instanceof String) {
                if (((String) o).equals(MeshTVButtons.CHECKOUT.getButton())) {
                    CarlsonConfirm.dialog(getResources().getString(R.string.confirmcheckout), .35, .25).show(getSupportFragmentManager(), "checkoutconfirm");
                }
            }
        }

        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public void onSelected(Object o) {

    }

}
