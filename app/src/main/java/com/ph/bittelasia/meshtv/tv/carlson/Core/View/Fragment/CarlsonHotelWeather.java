package com.ph.bittelasia.meshtv.tv.carlson.Core.View.Fragment;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.TextView;


import com.ph.bittelasia.meshtv.tv.carlson.R;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Annotation.View.General.Layout;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Annotation.View.General.TimedMethod;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Annotation.View.Widget.BindWidget;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Preference.Control.Listener.MeshWeatherListener;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Preference.Control.Manager.MeshTVPreferenceManager;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Preference.Model.MeshWeatherLocal;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Util.ResourceManager.MeshResourceManager;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Util.Util.MeshCountryConverter;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Util.Util.MeshTVTimeManager;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Object.Query.MeshValuePair;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.View.Fragment.MeshTVFragment;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Control.MeshRealmListener;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Control.MeshRealmManager;
import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Model.Weather.MeshWeatherV2;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by mars on 1/22/18.
 */
@Layout(R.layout.carlson_weather_info_layout)
public class CarlsonHotelWeather extends MeshTVFragment<MeshWeatherV2> implements MeshWeatherListener,MeshRealmListener {

    public static final String DATE_FORMAT = "MM/dd/yyyy";
    public static final String TIME_FORMAT = "hh:mm aa";

    @BindWidget(R.id.tv_weather_place)
    public TextView tv_weather_place;

    @BindWidget(R.id.tv_time)
    public TextClock tv_time;

    @BindWidget(R.id.tv_date)
    public TextView tv_date;

    @BindWidget(R.id.tv_temp)
    public TextView tv_temp;

    @BindWidget(R.id.iv_weather_icon)
    public ImageView iv_weather_icon;


    MeshWeatherLocal local;


    //----------------------------------------------------------------------------------------------
    //==============================================================================================
    //==============================================Timed===========================================
    @TimedMethod(delay = 30000)
    public void timeTicker()
    {
        update();
    }
    //==============================================================================================
    //==============================================LifeCycle=======================================

    @Override
    public void onResume() {
        super.onResume();
        MeshValuePair vp = new MeshValuePair(MeshWeatherV2.TAG_COUNTRY, MeshTVPreferenceManager.getHotelCountry(getActivity()));
        vp.setString(true);
        MeshRealmManager.retrieve(MeshWeatherV2.class,this,vp);
//        MeshRealmManager.retrieve(MeshWeatherV2.class,this);
    }

    //==============================================================================================
    //==============================================Method==========================================
    public void update()
    {


        if(tv_date!=null&&tv_time!=null)
        {
            try
            {
                MeshResourceManager.displayLiveImageFor(getActivity(),iv_weather_icon,MeshTVPreferenceManager.getHotelWeatherWeatherIcon(getActivity()));
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    //==============================================================================================
    //=============================================MeshTVFragment===================================
    @Override
    protected void onDrawDone(View view)
    {
        update();
    }
    @Override
    protected void onDataUpdated(ArrayList<MeshWeatherV2> arrayList) {

    }
    @Override
    protected void onNewData(Object o) {

    }
    @Override
    protected void onDataUpdated(Object o, int i) {

    }
    @Override
    protected void onDeleteData(int i) {

    }
    @Override
    protected void onClearData() {

    }
    @Override
    protected void onDataNotFound(Class aClass) {

    }
    @Override
    protected void refresh() {

    }
    @Override
    protected void update(MeshWeatherV2 meshWeatherLocal)
    {

    }
    //==============================================================================================
    //=====================================MeshWeatherListener========================================
    @Override
    public void onUpdateForWeather(MeshWeatherLocal meshWeatherLocal)
    {
        try {
            if (tv_weather_place != null && iv_weather_icon != null && tv_temp != null) {
                tv_weather_place.setText((MeshTVPreferenceManager.getHotelCity(getActivity())+", "+ MeshCountryConverter.getCountryFullname(MeshTVPreferenceManager.getHotelCountry(getActivity()))));
                tv_temp.setText((local.getTemp_cur() + " °C"));
                tv_date.setText((new SimpleDateFormat(DATE_FORMAT).format(new Date()).toUpperCase()));
                tv_time.setTimeZone(MeshTVTimeManager.getTimeZoneName(meshWeatherLocal.getCountry_code()));
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onRetrieved(Class aClass, ArrayList<Object> arrayList) {

//        for(Object o: arrayList)
//        {
//            Log.i("steward","===================================================");
//            Log.i("steward","weatherv2 country: "+((MeshWeatherV2)o).getCountry());
//            Log.i("steward","weatherv2 description: "+((MeshWeatherV2)o).getDescription());
//            Log.i("steward","weatherv2 icon: "+((MeshWeatherV2)o).getIcon());
//            Log.i("steward","weatherv2 location name: "+((MeshWeatherV2)o).getLoc_name());
//            Log.i("steward","weatherv2 wind speed: "+((MeshWeatherV2)o).getWind_speed());
//            Log.i("steward","weatherv2 humidity: "+((MeshWeatherV2)o).getHumidity());
//            Log.i("steward","weatherv2 id: "+((MeshWeatherV2)o).getId());
//            Log.i("steward","weatherv2 lat: "+((MeshWeatherV2)o).getLat());
//            Log.i("steward","weatherv2 location id: "+((MeshWeatherV2)o).getLoc_id());
//            Log.i("steward","weatherv2 timezone: "+((MeshWeatherV2)o).getTimezone());
//            Log.i("steward","weatherv2 temp cur: "+((MeshWeatherV2)o).getTemp_cur());
//            Log.i("steward","weatherv2 temp max: "+((MeshWeatherV2)o).getTemp_max());
//            Log.i("steward","weatherv2 temp min: "+((MeshWeatherV2)o).getTemp_min());
//            Log.i("steward","===================================================");
//        }

        local = new MeshWeatherLocal((MeshWeatherV2)arrayList.get(0));
        local.update();
        local.display();
    }

    @Override
    public void onFailed(Class aClass, String s) {

    }

    @Override
    public void onEmpty(Class aClass, String s) {

    }
    //==============================================================================================
}
