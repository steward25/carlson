package com.ph.bittelasia.meshtv.tv.carlson.Launcher.View.Activity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.ph.bittelasia.meshtv.tv.carlson.Airmedia.View.Activity.CarlsonAirMedia;
import com.ph.bittelasia.meshtv.tv.carlson.Core.View.Activity.CarlsonActivity;
import com.ph.bittelasia.meshtv.tv.carlson.Core.View.Fragment.CarlsonHotelWeather;
import com.ph.bittelasia.meshtv.tv.carlson.Core.View.Fragment.CarslonGuestInfo;
import com.ph.bittelasia.meshtv.tv.carlson.Launcher.View.Fragment.CarlsonGridFragmentV2;
import com.ph.bittelasia.meshtv.tv.carlson.R;
import com.ph.bittelasia.meshtv.tv.carlson.Settings.Model.SessionManager;
import com.ph.bittelasia.meshtv.tv.carlson.Temp.Model.Packages;
import com.ph.bittelasia.meshtv.tv.carlson.Temp.View.Toast.MeshTVToast;
import com.ph.bittelasia.meshtv.tv.mtvlib.AirMedia.Control.MeshTVAirmediaControl;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Annotation.View.Activity.ActivitySetting;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Annotation.View.Activity.AttachFragment;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Annotation.View.General.Layout;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Annotation.View.Widget.BindWidget;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Listener.BroadcastListeners.MeshTVAirmediaListener;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Listener.BroadcastListeners.MeshTVPackageListener;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.Control.Listener.MeshTVFragmentListener;
import com.ph.bittelasia.meshtv.tv.mtvlib.Core.View.Activity.MeshTVActivity;
import com.ph.bittelasia.meshtv.tv.mtvlib.PackageManager.Control.MeshTVGenericPackageListener;
import com.ph.bittelasia.meshtv.tv.mtvlib.PackageManager.Control.MeshTVPackageManager;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
@Layout(R.layout.carlson_launcher_layout)
@ActivitySetting()
public class CarlsonLauncher extends CarlsonActivity implements MeshTVFragmentListener, MeshTVAirmediaListener,MeshTVPackageListener,MeshTVGenericPackageListener {

    CarlsonGridFragmentV2 grid;
    String              display;
    BroadcastReceiver   receiver;


    CarslonGuestInfo guestInfo;
    CarlsonHotelWeather hotelWeather;


    @AttachFragment(container = R.id.ll_location,tag="location",order = 2)
    public Fragment attachHotelWeather()
    {
        hotelWeather=new CarlsonHotelWeather();
        return hotelWeather;
    }

    @AttachFragment(container = R.id.ll_guest,tag = "guest",order=3)
    public Fragment attachHomeWeather()
    {
        guestInfo=new CarslonGuestInfo();
        return  guestInfo;
    }

    public String allCaps(String value)
    {
        StringBuilder sb = new StringBuilder(value);
        for (int index = 0; index < sb.length(); index++) {
            char c = sb.charAt(index);
            if (Character.isLowerCase(c)) {
                sb.setCharAt(index, Character.toUpperCase(c));
            }
        }
        return sb.toString();
    }

    @AttachFragment(container = R.id.carlson_grid,tag="grid",order=0)
    public Fragment attachFragment()
    {
        grid=new CarlsonGridFragmentV2();
        return grid;
    }

    @Override
    public void airmediaReady() {
        Log.i("steward","lready");
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("toast");
        if(fragment != null)
        {
            MeshTVAirmediaControl.disableToast(this);
            Intent mIntent = new Intent(this, CarlsonAirMedia.class);
            mIntent.putExtra("ready", true);
            startActivity(mIntent);
        }
    }

    @Override
    public void airmediaNotReady() {
        Log.i("steward","lnot ready");
    }


    @Override
    public void appInstalled(String s) {
        if(s.equals(Packages.AIRMEDIA))
        {
            Log.i("steward","airmedia installed");
            Intent mIntent = new Intent(this, CarlsonAirMedia.class);
            startActivity(mIntent);
        }
    }

    @Override
    public void appUnInstalled(String s) {
        try
        {
            if (s.equals(Packages.AIRMEDIA)) {
                Log.i("steward", "airmedia was uninstalled");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public  boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try
        {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        }
        catch (PackageManager.NameNotFoundException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    public void appLaunch(String packageName)
    {
        Intent i;
        i = getPackageManager().getLaunchIntentForPackage(packageName);
        if(i!=null)
        {
            String packageString = i.getComponent().getPackageName();
            String mainActivity = i.getComponent().getClassName();
            i.addCategory(Intent.CATEGORY_LAUNCHER);
            i.setComponent(new ComponentName(packageString, mainActivity));
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            startActivity(i);
        }
    }


    public void appLaunch(Class classname)
    {
        Intent i;
        i = new Intent(this, classname);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(i);
    }

    @Override
    public void onInstalled(String s) {
        try
        {
            if(s.equals(Packages.FACEBOOK))
                appLaunch(s);
            if (s.equals(Packages.ENEWS))
                appLaunch(s);
            if (s.equals(Packages.NETFLIX))
                appLaunch(s);
            if (s.equals(Packages.TWITTER))
                appLaunch(s);
            if (s.equals(Packages.SPOTIFY))
                appLaunch(s);
            Log.i("steward",s+" installed");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onUninstalled(String s) {
        if(s.equals(Packages.FACEBOOK))
            Log.i("steward",Packages.FACEBOOK+" was uninstalled");
        if(s.equals(Packages.ENEWS))
            Log.i("steward",Packages.ENEWS+" was uninstalled");
        if(s.equals(Packages.NETFLIX))
            Log.i("steward",Packages.NETFLIX+" was uninstalled");
        if(s.equals(Packages.TWITTER))
            Log.i("steward",Packages.TWITTER+" was uninstalled");
        if(s.equals(Packages.SPOTIFY))
            Log.i("steward",Packages.SPOTIFY+" was uninstalled");
    }

    @Override
    public void onResume() {
        super.onResume();
        try
        {
            receiver = MeshTVPackageManager.listen(this, this);
            MeshTVAirmediaControl.uninstall(this);
        }
        catch (Exception e)
        {
          e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        MeshTVPackageManager.ignore(this,receiver);
    }

    @Override
    public void onClicked(Object o) {
        if(o instanceof String)
        {
            display=(String) o;
            if(display.equals(Packages.DISPLAY_AIRMEDIA))
            {
                MeshTVAirmediaControl.install(this);
                MeshTVToast.makeText(this,R.layout.toast,"Preparing Airmedia App...", Toast.LENGTH_LONG).show();
            }else {
                Log.i("steward", "class name: " + ((String) o));
                try {
                    Class<?> cls = Class.forName(((String) o));
                    appLaunch(cls);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onSelected(Object o) {

    }
}
