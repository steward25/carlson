package com.ph.bittelasia.meshtv.tv.carlson.Temp.Model;

import com.ph.bittelasia.meshtv.tv.mtvlib.Realm.Model.Message.MeshMessage;

import io.realm.Realm;

/**
 * Created by ramil on 3/5/18.
 */

public abstract class MeshMessageRead {

    public static int getUnreadMessage()
    {
        return Realm.getDefaultInstance().where(MeshMessage.class).equalTo("messg_status", Integer.valueOf(1)).findAll().size();
    }
}
