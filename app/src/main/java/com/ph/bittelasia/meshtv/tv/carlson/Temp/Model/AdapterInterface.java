package com.ph.bittelasia.meshtv.tv.carlson.Temp.Model;

/**
 * Created by ramil on 2/8/18.
 */

public interface AdapterInterface {
    public abstract void onClick(Object o);
    public abstract void onSelected(Object o);
}
